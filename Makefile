#
# Copyright (C) 2016-2017 Crans
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk
include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/version.mk

PKG_NAME:=crans
PKG_VERSION:=2.0
PKG_RELEASE:=1
PKG_LICENSE:=GPL-2.0

PKG_BUILD_DEPENDS:=base-files kmod-mac80211 dropbear !TARGET_ar71xx_generic_TLWR841:monit TARGET_ar71xx_generic_TLWR841:monit-nossl

include $(INCLUDE_DIR)/package.mk

SECRET:=$(shell PYTHONPATH=/etc/crans/secrets/ python -c "import secrets; print secrets.radius_eap_key")

MODEL=

WAN:=$(CONFIG_PACKAGE_CRANS_AP_WAN)
RADIUS:=$(CONFIG_PACKAGE_CRANS_RADIUS_SERVER)
CHANNEL_2_4:=$(CONFIG_PACKAGE_CRANS_CHANNEL_2_4)
CHANNEL_5:=$(CONFIG_PACKAGE_CRANS_CHANNEL_5)

# Les VLANs dynamiques ont-ils été activés
ifeq ($(CONFIG_PACKAGE_CRANS_DYNAMIC_VLAN),y)
  DYNAMIC_VLAN:=".dynamic"
else
  DYNAMIC_VLAN:=".static"
endif

# Pour installer les confs spécifiques, il faut regarder quel est le type de borne ciblé
ifeq ($(CONFIG_TARGET_ar71xx_generic_ESR1750),y)
  MODEL:="esr1750"
endif
ifeq ($(CONFIG_TARGET_ar71xx_generic_UBNTUNIFIACLITE),y)
  MODEL:="uap_ac_lite"
endif
ifeq ($(CONFIG_TARGET_ar71xx_generic_TLWR841),y)
  MODEL:="tl_wr841nd"
endif
ifndef MODEL
  MODEL:="default"
endif

define Package/crans
  SECTION:=base
  CATEGORY:=Crans configuration
  TITLE:=Crans-specific configuration for OpenWrt
  DEPENDS:=+base-files +kmod-mac80211 +dropbear +!TARGET_ar71xx_generic_TLWR841:monit +TARGET_ar71xx_generic_TLWR841:monit-nossl
  VERSION:=$(PKG_RELEASE)
  MENU:=1
endef

define Package/crans/config
	if PACKAGE_crans
		config PACKAGE_CRANS_CHANNEL_2_4
			depends on PACKAGE_crans
			int "2.4 GHz band channel"
			default 11
			help
				Select channel to use for the 2.4 GHz band Wi-Fi
		config PACKAGE_CRANS_CHANNEL_5
			depends on PACKAGE_crans
			int "5 GHz band channel"
			default 36
			help
				Select channel to use for the 5 GHz band Wi-Fi
		config PACKAGE_CRANS_RADIUS_SERVER
			depends on PACKAGE_crans
			string "RADIUS server IP address"
			default 10.231.148.11
		config PACKAGE_CRANS_DYNAMIC_VLAN
			depends on PACKAGE_crans
			bool "Add support for dynamic VLANs"
			default n
			help
				Select this option to add dynamic VLAN support
				to the generated OpenWRT image.
				This allows several networks to share the same SSID by assigning
				different VLANs to clients, depending on RADIUS server's replies.
		if TARGET_ar71xx_generic_TLWR841
			config PACKAGE_CRANS_AP_WAN
				depends on PACKAGE_CRANS_DYNAMIC_VLAN
				string "Access Point WAN interface"
				default eth1
		endif
		if !TARGET_ar71xx_generic_TLWR841
			config PACKAGE_CRANS_AP_WAN
				depends on PACKAGE_CRANS_DYNAMIC_VLAN
				string "Access Point WAN interface"
				default eth0
		endif
	endif
endef

define Package/crans/description
  Configuration spécifique au Crans
endef

define Package/base-files/conffiles
  /etc/banner
  /etc/openwrt_version
  /etc/monitrc
  /etc/config/dhcp
  /etc/config/network
  /etc/config/system
  /etc/config/dropbear
  /etc/config/wireless
  /etc/dropbear/
  /etc/dropbear/authorized_keys
  /etc/monit
  /etc/uci-defaults/dhcp.crans
  $(call $(TARGET)/conffiles)
endef

define Package/crans/install
	$(INSTALL_DIR) $(1)/etc/
	$(INSTALL_DATA) files/etc/banner $(1)/etc/banner
	$(INSTALL_DATA) files/etc/openwrt_version $(1)/etc/openwrt_version
	$(INSTALL_DATA) files/etc/monitrc $(1)/etc/monitrc
	$(INSTALL_DIR) $(1)/etc/config/
	$(INSTALL_DATA) files/etc/config/dhcp $(1)/etc/config/dhcp
	$(INSTALL_DATA) files/etc/config/network.$(MODEL) $(1)/etc/config/network
	$(INSTALL_DATA) files/etc/config/system.$(MODEL) $(1)/etc/config/system
	$(INSTALL_DATA) files/etc/config/dropbear $(1)/etc/config/dropbear
	install -d -m0700 $(1)/etc/dropbear/
	$(INSTALL_CONF) files/etc/dropbear/authorized_keys $(1)/etc/dropbear/authorized_keys
	$(INSTALL_DIR) $(1)/usr/share/watch_net/
	$(INSTALL_BIN) files/usr/share/watch_net/watch_net.sh $(1)/usr/share/watch_net/watch_net.sh
	$(INSTALL_DIR) $(1)/lib/wifi/
	$(INSTALL_BIN) files/lib/wifi/mac80211.sh$(DYNAMIC_VLAN) $(1)/lib/wifi/mac80211.sh
	$(INSTALL_DIR) $(1)/etc/uci-defaults/
	$(INSTALL_BIN) files/etc/uci-defaults/dhcp.crans $(1)/etc/uci-defaults/dhcp.crans
	$(INSTALL_DIR) $(1)/lib/netifd/
	$(INSTALL_BIN) files/lib/netifd/dhcp.script $(1)/lib/netifd/dhcp.script

	$(VERSION_SED) \
		$(1)/etc/banner \
		$(1)/etc/openwrt_version

	$(SED) s/%%iface%%/$(WAN)/g $(1)/lib/wifi/mac80211.sh
	$(SED) s/%%eap_radius_key%%/$(SECRET)/g $(1)/lib/wifi/mac80211.sh
	$(SED) s/%%channel_2_4%%/$(CHANNEL_2_4)/g $(1)/lib/wifi/mac80211.sh
	$(SED) s/%%channel_5%%/$(CHANNEL_5)/g $(1)/lib/wifi/mac80211.sh
	$(SED) s/%%radius_server%%/$(RADIUS)/g $(1)/lib/wifi/mac80211.sh
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
endef

define Build/Compile/Default

endef
Build/Compile = $(Build/Compile/Default)

$(eval $(call BuildPackage,crans))
