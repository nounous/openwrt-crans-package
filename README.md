Package OpenWrt pour le Crans
=============================

Ce dépôt contient l'ensemble des fichiers de configuration du Crans à
inclure dans les images OpenWrt déployées sur les bornes Wi-Fi.

Prérequis
---------

- Git
- Un dépôt OpenWrt, par exemple [https://github.com/openwrt/openwrt.git](https://github.com/openwrt/openwrt.git)(Miroir du dépôt OpenWrt)

Installation
------------

Pour installer le package, il suffit de le cloner dans le dossier `package`
de votre dépôt OpenWrt:


    $ git clone https://gitlab.crans.org/nounous/openwrt-crans-package.git crans


Certaines dépendances doivent être satisfaites pour que le paquet soit intégré
à l'image. On installe donc tous les paquets possibles depuis les dépôts feeds:


    $ ./scripts/feeds update
    $ ./scripts/feeds install -a


Reste à choisir les options de compilation __sans oublier de sélectionner le
paquet Crans -> Crans configuration__, puis à compiler l'image :


    $ make menuconfig   # Ne pas oublier le paquet Crans ici
    $ make -j9


Ajout d'un fichier de configuration
-----------------------------------

1.  Ajout d'un fichier `network.<modèle de borne>`

L'ajout d'un fichier de configuration réseau pour une borne est nécéssaire
lorsque celle-ci utilise une autre interface réseau que `eth0`, ou bien si elle
dispose d'un switch. (Concernant les switches, [https://wiki.openwrt.org/doc/uci/network/switch](la doc sur OpenWrt.org))

Une fois le fichier placé dans `files/etc/config/`, il faut modifier le
Makefile de façon à ce que ce fichier soit pris à la place du fichier par
défaut lors de la compilation. Cela se fait en rajoutant un test au début
de ce dernier:


        ...
        ifeq ($(CONFIG_TARGET_<option correspondante dans le .config>),y)
            MODEL +="<modèle de borne>"
        endif
        ifndef MODEL
            MODEL:="default"
        endif

2.  Ajout d'un dossier

Pour ajouter un dossier, il faut écrire dans la section `Package/crans/install`
l'instruction

    $(INSTALL_DIR) $(1)/<chemin absolu de la destination finale>

3.  Ajout d'un fichier de configuration

Pour ajouter un fichier de configuration, il faut:
    - Mettre le fichier au bon endroit dans le files/
    - Ajouter dans la section `Package/crans/install` l'instruction

    $(INSTALL_DATA) files/<emplacement du fichier dans le package> $(1)/<chemin absolu de la destination finale>

4.  Ajout d'un script/binaire

Pour ajouter un exécutable, il faut:
    - Le mettre au bon endroit dans le files/
    - Ajouter dans la section `Package/crans/install` l'instruction

    $(INSTALL_BIN) files/<emplacement du fichier dans le package> $(1)/<chemin absolu de la destination finale>
